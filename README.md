**Bakersfield free obgyn clinic**

Bakersfield Free Obgyn Clinic is the first integrated student-coordinated free women's health clinic in the world.
Please Visit Our Website [Bakersfield free obgyn clinic](https://obgynbakersfield.com/free-obgyn-clinic.php) for more information.

---

## Our free obgyn clinic in Bakersfield 

The Bakersfield Free OBGYN Clinic is a collaboration with the New York University School of Medicine, 
the Reproductive Health Access Initiative, and the Center for Family Health. 
Two Saturdays a month, the Women's Health Free Clinic is open; ask for a schedule, please.
